<?php

namespace App\Http\Controllers;

use App\Telegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;

class BotController extends Controller
{
    public function index(){
      //  $telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
        $php_errormsg = false;

        $result = [];

        $link_arr = [];

        $token = request('telegram_token') ? request('telegram_token') : env('TELEGRAM_BOT_TOKEN');



        $link_arr = [];

        try {
            $telegram = new Api(request('telegram_token'));

            //$telegram->removeWebhook();

            $result = $telegram->getUpdates();
            $result = array_reverse($result);
        }
        catch (\Exception $e){
            $php_errormsg = true;
        }



        foreach ($result as $data){
            preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $data->message->text, $match);
            if (isset($match[0]) &&  count($match[0]) > 0){
                $link_arr[] = $data->message->text;
            }

        }


//        foreach ($result as $data){}
//
//        return $telegram->getUpdates();
//
////        $response = $telegram->setWebhook([
////            'url' => 'https://mediusware.com/demo/telebot/public/'.env('TELEGRAM_BOT_TOKEN').'/webhook'
////        ]);
//
//        return json_encode($response);
//
////        $response = $telegram->getMe();
////
////        return $response;
////
////        $botId = $response->getId();
////        $firstName = $response->getFirstName();
////        $username = $response->getUsername();
///

        return view('list' , compact('link_arr' , 'php_errormsg'));

    }

    public function manage($token){

        $data = new Telegram;
        $data->response = json_encode(request()->all());
        $data->token = 'sample token';
        $data->save();
    }
}
